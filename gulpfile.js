// gulpfile.js
const gulp = require("gulp")
const sass = require("gulp-sass")
const cleanCSS = require('gulp-clean-css')
const terser = require('gulp-terser');
const rename = require("gulp-rename")
const sourcemaps = require('gulp-sourcemaps')
const imagemin = require('gulp-imagemin')
//const browserSync  = require('browser-sync').create();

// SCSS Task
function scss() {
  return (
      gulp
          .src("assets/scss/main.scss")
          .pipe(sourcemaps.init())
          .pipe(sass())
          .on("error", sass.logError)
          .pipe(sourcemaps.write())
          .pipe(gulp.dest("dist"))
  )
}

// CSS minify
function cssmin() {
  return (
    gulp
        .src("dist/main.css")
        .pipe(sourcemaps.init())
        .pipe(cleanCSS())
        .pipe(rename("main.min.css"))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest("dist"))
  )
}

// JS minify
function jsmin() {
  return (
    gulp
        .src("assets/js/script.js")
        .pipe(sourcemaps.init())
        .pipe(terser())
        .pipe(rename("script.min.js"))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest("dist"))
  )
}

// Image optimization
function image() {
  return (
      gulp
        .src('assets/images/*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/images'))
      )
}


// // Browser Sync tasks
// // Static server
// function browser() {
//     browserSync.init({
//         server: {
//             baseDir: "./"
//         }
//     });
// }


// Watch task
function watch() {
    gulp.watch(
      'assets/**/*',
      gulp.series(scss, cssmin, jsmin)
    )
}

// // Dev task
// function dev() {
//     gulp.series(browser, watch)
// }

// Export tasks
exports.scss = scss
exports.cssmin = cssmin
exports.jsmin = jsmin
exports.image = image
exports.watch = watch
exports.default = gulp.series(scss, cssmin, jsmin)
// exports.dev = dev
